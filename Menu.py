class Menu():
    def __init__(self, name, description="", parent=None, parent_menu=None, root_objs=[]):
        """
        Initialise Menu Attributes 
        name: String representing a printable name
        description: String representing a description if required
        parent: Item Object that lead to this current Menu Object if required
        parent_menu: Menu Object that was previous to the current Menu Object if required
        root_objs: List of Item Objects to display as menu options
        """
        self.name = name
        self.description = description
        self.root_objs = root_objs
        self.construct = {}
        self.parent = parent
        self.parent_menu = parent_menu

    def __repr__(self):
        return f'{self.name}\n----------------\n{self.description}\n----------------'
        
    def add_item(self, obj):
        """
        Add to Item object to display as a menu option
        """
        self.root_objs.append(obj)
    
    def construct_menu(self):
        """
        Construct a reference dictionary for Menu Text and Menu Selection Actions
        """
        if self.parent == None:
            self.construct['Text'] = f"{self.name}\n---------------\nMain Menu\n---------------\n"
        else:
            self.construct['Text'] = f"{self.name}\n---------------\n{self.parent.name}\n---------------\n"
        item_val_i = 0

        for i in self.root_objs:
            item_val_i += 1
            self.construct[str(item_val_i)] = i
            self.construct['Text'] = f"{self.construct['Text']}{item_val_i}. {i.name}\n"
        if self.parent is None:
            try:
                get_ipython
                self.construct['X'] = None
            except:
                self.construct['X'] = quit
            self.construct['Text'] = f"{self.construct['Text']}X. Exit"
        else:
            self.construct['X'] = self.parent_menu.prompt
            self.construct['Text'] = f"{self.construct['Text']}X. Previous Menu"

    def display(self, text):
        """
        Display Menu Text
        text: String construction of menu to be displayed
        """
        print(text)
    
    def clear_console(self):
        """
        Clear the console be it iPython shell, Linux, Mac or Windows
        """
        try:
            get_ipython
            from IPython.display import clear_output
            clear_output()
        except:
            import os
            os.system('cls' if os.name == 'nt' else 'clear')
    
    def prompt(self):
        """
        Main method for Triggering necessary Menu Methods for use
        """
        self.clear_console()
        self.construct_menu()
        self.display(self.construct['Text'])
        choice = input("Select a Menu Item: ")
        if choice.upper() == 'X':
            if self.construct[choice.upper()] is None:
                pass
            else:
                self.construct[choice.upper()]()
        elif choice not in self.construct.keys():
            self.prompt()
        elif len(self.construct[choice].children) > 0:
            Sub_Menu = Menu(self.name, self.description, self.construct[choice], 
                            self, self.construct[choice].children)
            Sub_Menu.prompt()
        else:
            self.construct[choice].func()
            self.prompt()
            
class Item():
    def __init__(self, name, description="", parent=None, func=None):
        """
        Initialise Item Attributes
        name: String representing a printable name
        description: String representing a description if required
        parent: Item Object that this Item Object is a sub option for
        """
        self.name = name
        self.description = description
        self.children = []
        if parent is None:
            self.parent = None
        else:
            if parent.func is None:
                self.add_parent(parent)
            else:
                raise ValueError("Can't be a child if the parent has an action/function assigned")
        self.func = func
        
    def add_child(self, child):
        """
        Add Item Object as a sub option to this Item Object
        child: Item Object that is to be a sub option to this Item Object
        """
        self.children.append(child)
        child.parent = self
        
    def add_parent(self, parent):
        """
        Add this item object as a sub option to another Item Object
        parent: Item Object that this Item Object is a sub option for
        """
        self.parent = parent
        parent.children.append(self)
    
    def add_action(self, func):
        """
        Add action to occur when calling this menu item
        Should only be available when it doesn't have children
        func: Function to be executed when the item is called
        """
        if len(self.children) == 0:
            self.func = func
        else:
            raise ValueError("You can't have an action/function if an Item Object has children")

if __name__ == "__main__":
    pass